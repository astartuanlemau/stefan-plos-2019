
# coding: utf-8

# In[1]:


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
get_ipython().magic('matplotlib inline')


# In[18]:





# In[20]:


subjectName=['s'+ str(i) for i in list(range(1,12))]


# In[21]:


subjectName


# In[22]:


header=['target_val','target_aro']+subjectName


# In[94]:


dfDataAro=pd.DataFrame.from_csv('data_aro.csv',header=None,index_col=None)

dfDataAro.columns=header

dfA=dfDataAro.set_index(['target_val','target_aro'])

# dfA - AROUSAL


# In[92]:



dfDataVal=pd.DataFrame.from_csv('data_val.csv',header=None,index_col=None)

dfDataVal.columns=header

dfV=dfDataVal.set_index(['target_val','target_aro'])
# dfV - VALENCE


# In[209]:


pdTarget=dict()


# In[211]:


SCALE=8
for index,targetVA in enumerate(dfV.index):
    print(targetVA)
    pdTarget[targetVA]=pd.concat([dfV.loc[targetVA],dfA.loc[targetVA]],axis=1).T    .reset_index()    .drop(['level_0','level_1'],axis=1)    .T    /SCALE
    pdTarget[targetVA].columns=['0_valence','1_arousal']
    pdTarget[targetVA].plot.scatter(
    x='0_valence',y='1_arousal'
    )
    plt.title('(v,a)='+str(targetVA))
    plt.xlim(0,8/SCALE)
    plt.ylim(0,8/SCALE)
    plt.show()


# In[212]:


dfV.index


# In[213]:


pdStat=pd.DataFrame()
for target in dfV.index:
    print(target)
    print(pdTarget[target].describe()[1:3])
    print('\n')
    pdTemp=pdTarget[target].describe()[1:3].apply(
        lambda col:  '{:.2f}+/-{:.2f}'.format(col[0], col[1]),axis=0
    )
    pdTemp['targetVA']=str(target)
    pdStat=pd.concat([pdStat,pdTemp],axis=1)


# In[214]:


pdStat.T.set_index('targetVA')


# In[215]:


dfV.index


# In[221]:


dfV.index


# In[ ]:


SCALE=8
for index,targetVA in enumerate(dfV.index):
    print(targetVA)
    pdTarget[targetVA]=pd.concat([dfV.loc[targetVA],dfA.loc[targetVA]],axis=1).T    .reset_index()    .drop(['level_0','level_1'],axis=1)    .T    /SCALE
    pdTarget[targetVA].columns=['0_valence','1_arousal']
    pdTarget[targetVA].plot.scatter(
    x='0_valence',y='1_arousal'
    )
    plt.title('(v,a)='+str(targetVA))
    plt.xlim(0,8/SCALE)
    plt.ylim(0,8/SCALE)
    plt.show()

